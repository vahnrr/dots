# [i3] Simple workstation

![theme](examples/theme.png)

| Type of programme     | Package used                                      |
|-----------------------|---------------------------------------------------|
| Bar                   | [Polybar](https://github.com/polybar/polybar)     |
| Compositor            | [Compton](https://github.com/chjj/compton)        |
| File manager          | [Nemo](https://github.com/linuxmint/nemo)         |
| GTK theme             | See the `GTK theme` section bellow                |
| Icon theme            | [Paper](https://snwh.org/paper)                   |
| Menus / App launcher  | [Rofi](https://github.com/davatorium/rofi)        |
| Music daemon          | [Mpd](https://github.com/MusicPlayerDaemon/MPD)   |
| Notifications         | [Dunst](https://github.com/dunst-project/dunst)   |
| Shell                 | [Zsh](https://www.zsh.org/)                       |
| Text editor           | [Neovim](https://github.com/neovim/neovim)        |
| Window manager        | [i3-gaps](https://github.com/Airblader/i3)        |
| Web browser           | [Firefox](https://www.mozilla.org/en-US/firefox/) |

# Dunst

![dunst](examples/dunst.png)

# Firefox

**Warning:** since Firefox 67 there's been ugly stuff happening to loading tabs, so don't expect a perfect result from this custom Firefox css config.

![firefox](examples/firefox.png)

# GTK theme

Using [oomox](https://github.com/themix-project/oomox) I've made a version of the [Materia GTK theme](https://github.com/nana-4/materia-theme) (I only changed the `Selected Background` variable), so that it matches the steel-blue color I use everywhere.

# Nemo

Here's [nemo](https://github.com/linuxmint/nemo) using the GTK theme mentionned above, I manually tweaked that theme a little to get that blue headerbar.

![nemo](examples/nemo.png)

# Neovim

If you copy my config you'll need to run the `:PlugInstall` command to download the plugins used in the config.

![neovim](examples/neovim.png)

# Polybar

## basic

### Left monitor

Normal look:
![polybar_basic_left](examples/polybar_basic_left.png)

Urgent look:
![polybar_basic_left_urgent](examples/polybar_basic_left_urgent.png)

### Right monitor

Normal look:
![polybar_basic_right](examples/polybar_basic_right.png)

# Rofi

## drun

![rofi_drun](examples/rofi_drun.png)

## Menus

Those are really simple bash scripts for rofi using the `-dmenu` argument. Please see [this repo](https://gitlab.com/vahnrr/rofi-menus) for more information about these menus.

### Layout

This menu changes the layout of windows in i3, there's only tiled or tabed because those are the only ones I use.

![rofi_layout](examples/rofi_layout.png)

### Maim / Scrot

This menu can take screenshots in 3 different ways:
- The whole screen
- A rectangular area the user draws
- The active window

![rofi_maim](examples/rofi_maim.png)

**Note:** 2 versions exists, one using maim, the other using scrot.

### Mpd

This menu controls mpd through [mpc](https://github.com/MusicPlayerDaemon/mpc) commands.

![rofi_mpd](examples/rofi_mpd.png)

### Network

This menu is a replacement for the NetworkManager tray, it uses [networkmanager-dmenu](https://github.com/firecat53/networkmanager-dmenu).

![rofi_network](examples/rofi_network.png)

### Powermenu

This menu uses a custom lockscreen that calls [i3lock-color](https://github.com/PandorasFox/i3lock-color) and `systemctl` to execute power actions.

![rofi_powermenu](examples/rofi_powermenu.png)

# Zsh

- Prompt: [Spaceship](https://github.com/denysdovhan/spaceship-prompt)
- Colors: from [this](https://github.com/joshdick/onedark.vim) vim colorscheme

![zsh](examples/zsh.png)

