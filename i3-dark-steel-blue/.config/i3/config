#
# ██╗██████╗
# ██║╚════██╗
# ██║ █████╔╝
# ██║ ╚═══██╗
# ██║██████╔╝
# ╚═╝╚═════╝
#
# http://i3wm.org/docs/userguide.html


#            _   _   _
#   ___  ___| |_| |_(_)_ __   __ _ ___
#  / __|/ _ \ __| __| | '_ \ / _` / __|
#  \__ \  __/ |_| |_| | | | | (_| \__ \
#  |___/\___|\__|\__|_|_| |_|\__, |___/
#                            |___/
#

### Borders ###
set $border_size_float 4
# Borders are only set for floating windows
default_border pixel 0
default_floating_border pixel $border_size_float
new_window pixel 0
new_float normal $border_size_float
# Hide borders when there's only one window
hide_edge_borders smart

### Titlebar ###
# Font for window titles
font xft:Comfortaa 11
# Center the title
title_align center
# Make the window's title clearer and thicker
for_window [class=".*"] title_format "<b>%title</b>"
# Custom window titles
for_window [class="Gsimplecal"] title_format "<b>Calendar</b>"
for_window [class="Galculator"] title_format "<b>Calculator</b>"

### Floating ###
# Use the command `xprop WM_CLASS` to get a window's class
# Open specific applications in floating mode
set $float floating enable, border normal 4
for_window [class="bookmarks"] $float
for_window [class="File-roller"] $float
for_window [title="File Transfer*"] $float
for_window [class="Galculator"] $float
for_window [class="Gcolor3"] $float
for_window [class="Lightdm-settings"] $float
for_window [class="Manjaro Settings Manager"] $float
for_window [class="Nm-connection-editor"] $float
# Floating and sticky with window title
set $float_and_sticky floating enable sticky enable, border normal 4
for_window [class="Lxappearance"] $float_and_sticky
for_window [class="Nitrogen"] $float_and_sticky
for_window [class="qt5ct"] $float_and_sticky
# Floating without window title
set $float_no_title floating enable, border pixel 4
for_window [title="Floating Terminal"] $float_no_title
# Floating and sticky without window title
set $float_and_sticky_no_title floating enable sticky enable, border pixel 4
for_window [title="feh.*/tmp/qr_code.png$"] $float_and_sticky_no_title
# Floating, resized and centered
for_window [window_role="GtkFileChooserDialog"] resize set 900 600, move position center

### Gaps ###
set $gaps_inner 4
set $gaps_variation 4
# Set inner/outer gaps
gaps inner $gaps_inner
gaps outer 0
# Defines wether gaps are used when there's only one window (on meaning no)
smart_gaps on

### Workspaces ###
# Workspace back and forth (with/without active container)
workspace_auto_back_and_forth yes
# Workspace names
set $ws1 1
set $ws2 2
set $ws3 3
set $ws4 4
set $ws5 5
set $ws6 6
set $ws7 7
set $ws8 8
set $ws9 9
# Open applications on specific workspaces
assign [class="Firefox"]            $ws1
assign [class="Nemo"]               $ws2
assign [class="Syncthing GTK"]      $ws2
assign [class="jetbrains-idea-ce"]  $ws3
assign [class="cantata"]            $ws4
assign [class="vlc"]                $ws5
assign [class="discord"]            $ws6
assign [class="libreoffice"]        $ws7
assign [class="VirtualBox Manager"] $ws8
assign [class="Transmission-gtk"]   $ws9

### i3wm colors ###
set $accent #728cbb
set $dark   #282c34
set $urgent #de935f
# class					border  bg      fg      indic.  child_border
client.focused          $accent $accent $dark   $accent
client.focused_inactive $dark   $dark   $accent $dark
client.unfocused        $dark   $dark   $accent $dark
client.urgent           $urgent $urgent $dark   $urgent
client.placeholder      $dark   $dark   $urgent $dark
client.background       $dark


#       _             _
#   ___| |_ __ _ _ __| |_ _   _ _ __
#  / __| __/ _` | '__| __| | | | '_ \
#  \__ \ || (_| | |  | |_| |_| | |_) |
#  |___/\__\__,_|_|   \__|\__,_| .__/
#                              |_|
#

# Notifications
exec_always --no-startup-id dunst -config ~/.config/dunst/dunstrc
# Wallpaper
exec_always --no-startup-id nitrogen --restore
# Bar
exec_always --no-startup-id ~/.config/polybar/launch.sh
# Compositor
exec_always --no-startup-id compton &
# Flashing effect on window focus change
exec --no-startup-id flashfocus
# Music server
exec --no-startup-id mpd


#   _                _     _           _ _
#  | | _____ _   _  | |__ (_)_ __   __| (_)_ __   __ _ ___
#  | |/ / _ \ | | | | '_ \| | '_ \ / _` | | '_ \ / _` / __|
#  |   <  __/ |_| | | |_) | | | | | (_| | | | | | (_| \__ \
#  |_|\_\___|\__, | |_.__/|_|_| |_|\__,_|_|_| |_|\__, |___/
#            |___/                               |___/
#

# Set mod key (Mod1=<Alt>, Mod4=<Super>)
set $mod Mod4
set $Alt Mod1

# Use Mouse+$mod to drag floating windows
floating_modifier $mod

# Kill focused window
bindsym $mod+q                  kill
bindsym $mod+BackSpace          kill

# Quick fullscreen screenshot
# Note: Print requires 'Fn' to be triggered
bindsym Print                   exec --no-startup-id i3-scrot

# Volume keys
bindsym XF86AudioRaiseVolume    exec amixer set Master 5%+
bindsym XF86AudioLowerVolume    exec amixer set Master 5%-
bindsym XF86AudioMute           exec amixer set Master toggle

# Brightness keys
bindsym XF86MonBrightnessUp     exec xbacklight -inc 10
bindsym XF86MonBrightnessDown   exec xbacklight -dec 10

# Prompt a QR code with the content of the clipboard
bindsym $mod+Shift+q            exec --no-startup-id xclip -selection c -o | qrencode -s 8 -o /tmp/qr_code.png && feh /tmp/qr_code.png && rm /tmp/qr_code.png


### Window navigation / management ###
# Change focus
bindsym $mod+Left               focus left
bindsym $mod+Down               focus down
bindsym $mod+Up                 focus up
bindsym $mod+Right              focus right
# Move focused window
bindsym $mod+Shift+Left         move left
bindsym $mod+Shift+Down         move down
bindsym $mod+Shift+Up           move up
bindsym $mod+Shift+Right        move right
# Tiling horizontally / vertically to be used for the next window
bindsym $mod+h                  split h
bindsym $mod+v                  split v
# Flash the active window to make it stand out for a fraction of seconds
bindsym $mod+w                  exec --no-startup-id flash_window
# Toggle fullscreen mode for currently focused window
bindsym $mod+f                  fullscreen toggle
# Toggle normal / floating
bindsym $mod+Shift+space        floating toggle
# Change focus between normal / floating windows
bindsym $mod+space              focus mode_toggle
# Toggle sticky
bindsym $mod+Shift+s            sticky toggle
# Move the currently focused window to the scratchpad (minimise)
bindsym $mod+Shift+b            move scratchpad
# Show the next scratchpad window or hide the focused scratchpad window
bindsym $mod+b                  scratchpad show
# Gaps control
bindsym $mod+Shift+g            gaps inner current plus $gaps_variation
bindsym $mod+Ctrl+g             gaps inner current minus $gaps_variation
bindsym $mod+g                  gaps inner current set $gaps_inner

### Apps ###
# Terminal
bindsym $mod+Return             exec xfce4-terminal
# Floating terminal
bindsym $mod+Shift+Return       exec xfce4-terminal -T "Floating Terminal" --geometry 80x4
# App launcher (rofi)
bindsym $mod+d                  exec --no-startup-id rofi -no-lazy-grab -show drun -theme themes/drun.rasi
# Exit menu (rofi)
bindsym $mod+0                  exec --no-startup-id ~/.config/rofi/scripts/powermenu.sh
# Layout menu (rofi)
bindsym $mod+a                  exec --no-startup-id ~/.config/rofi/scripts/layout.sh
# Scrot menu (rofi)
bindsym $mod+s                  exec --no-startup-id ~/.config/rofi/scripts/scrot.sh
# Mpd menu (rofi)
bindsym $mod+m                  exec --no-startup-id ~/.config/rofi/scripts/mpd.sh
# Network menu (rofi)
bindsym $mod+n                  exec --no-startup-id python ~/.config/rofi/scripts/network.py
# Calculator
bindsym $mod+c                  exec galculator
# Workspaces specific applications
bindsym $mod+F1                 exec firefox --new-tab "about:newtab"
bindsym $mod+F2                 exec nemo
bindsym $Alt+F2                 exec syncthing-gtk
bindsym $mod+F4                 exec cantata
bindsym $mod+F5                 exec vlc
bindsym $mod+F6                 exec discord
bindsym $mod+F7                 exec libreoffice
bindsym $mod+F8                 exec virtualbox
bindsym $mod+F9                 exec transmission-gtk

### Workspaces ###
# Move current workspace to right / left monitor
bindsym $mod+$Alt+Right         move workspace to output right
bindsym $mod+$Alt+Left          move workspace to output left
# Navigate workspaces next / previous
bindsym $mod+Ctrl+Right         workspace next
bindsym $mod+Ctrl+Left          workspace prev
# Switch to workspace
bindsym $mod+1                  workspace $ws1
bindsym $mod+2                  workspace $ws2
bindsym $mod+3                  workspace $ws3
bindsym $mod+4                  workspace $ws4
bindsym $mod+5                  workspace $ws5
bindsym $mod+6                  workspace $ws6
bindsym $mod+7                  workspace $ws7
bindsym $mod+8                  workspace $ws8
bindsym $mod+9                  workspace $ws9
# Move focused container to workspace
bindsym $mod+Ctrl+1             move container to workspace $ws1
bindsym $mod+Ctrl+2             move container to workspace $ws2
bindsym $mod+Ctrl+3             move container to workspace $ws3
bindsym $mod+Ctrl+4             move container to workspace $ws4
bindsym $mod+Ctrl+5             move container to workspace $ws5
bindsym $mod+Ctrl+6             move container to workspace $ws6
bindsym $mod+Ctrl+7             move container to workspace $ws7
bindsym $mod+Ctrl+8             move container to workspace $ws8
bindsym $mod+Ctrl+9             move container to workspace $ws9
# Move to workspace with focused container
bindsym $mod+Shift+1            move container to workspace $ws1; workspace $ws1
bindsym $mod+Shift+2            move container to workspace $ws2; workspace $ws2
bindsym $mod+Shift+3            move container to workspace $ws3; workspace $ws3
bindsym $mod+Shift+4            move container to workspace $ws4; workspace $ws4
bindsym $mod+Shift+5            move container to workspace $ws5; workspace $ws5
bindsym $mod+Shift+6            move container to workspace $ws6; workspace $ws6
bindsym $mod+Shift+7            move container to workspace $ws7; workspace $ws7
bindsym $mod+Shift+8            move container to workspace $ws8; workspace $ws8
bindsym $mod+Shift+9            move container to workspace $ws9; workspace $ws9


#                       _
#   _ __ ___   ___   __| | ___  ___
#  | '_ ` _ \ / _ \ / _` |/ _ \/ __|
#  | | | | | | (_) | (_| |  __/\__ \
#  |_| |_| |_|\___/ \__,_|\___||___/
#

### Resize mode ###
bindsym $mod+r \
    exec --no-startup-id notify-send --icon=/usr/share/icons/Paper/scalable/actions/image-crop-symbolic.svg \
        "Resizing mode" \
        "Use the arrow keys to resize the active window"; \
    mode "resize"
mode "resize" {
    # Pressing left will shrink the window’s width.
    bindsym Left                resize shrink width 10 px or 10 ppt
    # Pressing up will shrink the window’s height.
    bindsym Down                resize grow height 10 px or 10 ppt
    # Pressing down will grow the window’s height.
    bindsym Up                  resize shrink height 10 px or 10 ppt
    # Pressing right will grow the window’s width.
    bindsym Right               resize grow width 10 px or 10 ppt
    # Exit resize mode: Enter or Escape
    bindsym Return              mode "default"
    bindsym Escape              mode "default"
}

### Restart mode ###
bindsym $mod+x \
    exec --no-startup-id notify-send --icon=/usr/share/icons/Paper/scalable/actions/view-refresh-symbolic.svg \
        "Restart mode" \
        "<span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>D</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> dunst </span><span color='#333333'></span>  <span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>I</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> i3 </span><span color='#333333'></span>  <span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>P</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> polybar </span><span color='#333333'></span>  <span color='#728cbb'></span><span weight='heavy' color='#ffffff' background='#728cbb'>C</span><span color='#728cbb' background='#333333'></span><span color='#e2e2e2' background='#333333'> compton </span><span color='#333333'></span> "; \
    mode "restart"
mode "restart" {
    # Restart dunst (notifications)
    bindsym d                   exec --no-startup-id pkill dunst && dunst -config ~/.config/dunst/dunstrc; mode "default"
    # Restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
    bindsym i                   restart; mode "default"
    # Restart the polybar (the pretty bar up top)
    bindsym p                   exec --no-startup-id ~/.config/polybar/launch.sh; mode "default"
    # Restart compton (compositor)
    bindsym c                   exec --no-startup-id pkill compton && compton &; mode "default"
    # Exit restart mode: "Enter" or "Escape"
    bindsym Return              mode "default"
    bindsym Escape              mode "default"
}

# vim:ft=i3config
