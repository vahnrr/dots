#!/bin/bash

rofi_command="rofi -theme themes/mpd.rasi"

### Options ###
# Gets the current status of mpd (for us to parse it later on)
status="$(mpc status)"
# Defines the Play / Pause option content
if [[ $status == *"[playing]"* ]]; then
    play_pause=""
else
    play_pause=""
fi
# Display if repeat mode is on / off
if [[ $status == *"repeat: on"* ]]; then
    tog_repeat="凌"
elif [[ $status == *"repeat: off"* ]]; then
    tog_repeat="稜"
else
    tog_repeat=" Parsing error"
fi
# Display if random mode is on / off
if [[ $status == *"random: on"* ]]; then
    tog_random=""
elif [[ $status == *"random: off"* ]]; then
    tog_random=""
else
    tog_random=" Parsing error"
fi
# Unchanging ones
stop=""
next=""
previous=""
# Variable passed to rofi
options="$previous\n$play_pause\n$stop\n$next\n$tog_repeat\n$tog_random"

# Get the current playing song
current=$(mpc current)
# If mpd isn't running it will return an empty string, we don't want to display that
if [[ -z "$current" ]]; then
    current="-"
fi

# Spawn the mpd menu with the "Play / Pause" entry selected by default
chosen="$(echo -e "$options" | $rofi_command -p "$current" -dmenu -selected-row 1)"
case $chosen in
    $previous)
        mpc -q prev
        ;;
    $play_pause)
        mpc -q toggle
        ;;
    $stop)
        mpc -q stop
        ;;
    $next)
        mpc -q next
        ;;
    $tog_repeat)
        mpc -q repeat
        ;;
    $tog_random)
        mpc -q random
        ;;
esac

