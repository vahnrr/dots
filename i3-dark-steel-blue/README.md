# [i3] Dark steel-blue

![theme](examples/theme.png)

Wallpaper: [found here](https://www.wallpaperflare.com/static/120/833/239/blurry-lights-night-dark-wallpaper.jpg)

| Type of programme     | Package used                                      |
|-----------------------|---------------------------------------------------|
| Bar                   | [Polybar](https://github.com/polybar/polybar)     |
| Compositor            | [Compton](https://github.com/chjj/compton)        |
| File manager          | [Nemo](https://github.com/linuxmint/nemo)         |
| GTK theme             | See the `GTK theme` section bellow                |
| Icon theme            | [Paper](https://snwh.org/paper)                   |
| Menus / App launcher  | [Rofi](https://github.com/davatorium/rofi)        |
| Music daemon          | [Mpd](https://github.com/MusicPlayerDaemon/MPD)   |
| Notifications         | [Dunst](https://github.com/dunst-project/dunst)   |
| Shell                 | [Zsh](https://www.zsh.org/)                       |
| Text editor           | [Neovim](https://github.com/neovim/neovim)        |
| Window manager        | [i3-gaps](https://github.com/Airblader/i3)        |
| Web browser           | [Firefox](https://www.mozilla.org/en-US/firefox/) |

**Note:** I don't use borders for i3 windows, instead I use [this](https://github.com/fennerm/flashfocus) to get a flash effect on focus change.

# Dunst

![dunst](examples/dunst.png)

# Firefox

**Warning:** since Firefox 67 there's been ugly stuff happening to loading tabs, so don't expect a perfect result from this custom Firefox css config.

The base of this theme is [ShadowFox](https://github.com/overdodactyl/ShadowFox).

![firefox](examples/firefox.png)

# GTK theme

Using [oomox](https://github.com/themix-project/oomox) I've made a version of the [Materia GTK theme](https://github.com/nana-4/materia-theme) (I only changed the `Selected Background` variable), so that it matches the steel-blue color I use everywhere.

# Nemo

Here's [nemo](https://github.com/linuxmint/nemo) using the GTK theme mentionned above, I manually tweaked that theme a little to get that blue headerbar.

![nemo](examples/nemo.png)

# Neovim

If you copy my config you'll need to run the `:PlugInstall` command to download the plugins used in the config.

![neovim](examples/neovim.png)

# Polybar

## blocks_bar

Normal look:
![polybar_blocks_bar](examples/polybar_blocks_bar.png)

Urgent look:
![polybar_blocks_bar_urgent](examples/polybar_blocks_bar_urgent.png)

## top_bar

Normal look:
![polybar_top_bar](examples/polybar_top_bar.png)

Urgent look:
![polybar_top_bar_urgent](examples/polybar_top_bar_urgent.png)

# Rofi

## drun

![rofi_drun](examples/rofi_drun.png)

## Menus

Those are really simple bash scripts for rofi using the `-dmenu` argument.

### Layout

This menu changes the layout of windows in i3, there's only tiled or tabed because those are the only ones I use.

![rofi_layout](examples/rofi_layout.png)

### Mpd

This menu controls mpd through [mpc](https://github.com/MusicPlayerDaemon/mpc) commands.

![rofi_mpd](examples/rofi_mpd.png)

### Network

This menu is a replacement for the NetworkManager tray, it uses [networkmanager-dmenu](https://github.com/firecat53/networkmanager-dmenu).

![rofi_network](examples/rofi_network.png)

### Powermenu

This menu uses `xfce4-session-loggout` and `light-locker` to execute power actions.

![rofi_powermenu](examples/rofi_powermenu.png)

### Scrot

This menu can take screenshots in 3 different ways:
- A rectangular area the user draws
- The whole screen
- The active window

![rofi_scrot](examples/rofi_scrot.png)

# Zsh

- Prompt: [Spaceship](https://github.com/denysdovhan/spaceship-prompt)
- Colors: from [this](https://github.com/joshdick/onedark.vim) vim colorscheme

![zsh](examples/zsh.png)

