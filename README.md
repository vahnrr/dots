# dots

As a wise man once said:
> It ain't much, but it's honest work

# [i3] Dark steel-blue

This theme's details are documented [here](https://gitlab.com/vahnrr/dots/blob/master/i3-dark-steel-blue/README.md).

![theme](i3-dark-steel-blue/examples/theme.png)

# [i3] Simple workstation

This theme's details are documented [here](https://gitlab.com/vahnrr/dots/blob/master/i3-simple-workstation/README.md).

![theme](i3-simple-workstation/examples/theme.png)

